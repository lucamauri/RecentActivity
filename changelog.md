<a name="1.7"></a>
# 1.7 (2023-12-11)
## Fixes
* Improved `onParserFirstCallInit` as per [Manual:Parser functions](https://www.mediawiki.org/wiki/Manual:Parser_functions)

## Improvements
* One small improvement is the addition of this `changelog.md` file.
* `manifest_version` moved to version 2 as reccomended per [schema's documentation](https://www.mediawiki.org/wiki/Manual:Extension.json/Schema).

## Code
* Replacement of `$dbr->fetchRow` with `foreach {…}` cycles.
<!-- CHANGELOG SPLIT MARKER -->